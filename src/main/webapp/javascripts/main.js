requirejs.config({
    paths: {
        'jquery': 'https://cdn.bootcss.com/jquery/2.2.4/jquery',
        'bootstrap': 'https://cdn.bootcss.com/bootstrap/3.3.6/js/bootstrap',
        'angular': 'https://cdn.bootcss.com/angular.js/1.5.5/angular',
        'angular-ui-router': 'https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.3.1/angular-ui-router',
        'angular-resource': 'https://cdn.bootcss.com/angular-resource/1.5.5/angular-resource',
        'angular-touch': 'https://cdn.bootcss.com/angular-touch/1.5.7/angular-touch',
        'ui-bootstrap': 'https://cdn.bootcss.com/angular-ui-bootstrap/1.3.3/ui-bootstrap',
        'ui-bootstrap-tpls': 'https://cdn.bootcss.com/angular-ui-bootstrap/1.3.3/ui-bootstrap-tpls',
        'app': 'app',
        'routeResolver':'routeResolver',
        'httpService':'services/httpService',
        'indexController':'controllers/indexController'
    },
    //use shim because global variable is not amd supported
    shim:{
        'jquery': {
            exports:'jquery'
        },
        'bootstrap':{
            deps:['jquery'],
            exports:'bootstrap'
        },
        'angular':{
            exports:'angular'
        },
        'angular-ui-router':{
            deps:['angular'],
            exports:'angular-ui-router'
        },
        'angular-touch':{
            deps:['angular'],
            exports:'angular-route'
        },
        'ui-bootstrap':{
            deps:['angular'],
            exports:'ui-bootstrap'
        },
        'ui-bootstrap-tpls':{
            deps:['angular'],
            exports:'ui-bootstrap-tpls'
        }
    }
});

require(
    [
        'angular',
        'bootstrap',
        'ui-bootstrap',
        'ui-bootstrap-tpls',
        'app',
        'routeResolver',
        'httpService',
        'indexController'

    ],
    function(angular, app) {
        // angular.bootstrap(element, [modules], [config]);
        angular.bootstrap(document, ['admin_seed']);
    }
);
