define(['angular', 'angular-ui-router'], function (angular) {
    var app = angular.module('admin_seed', ['ui.router', 'ui.bootstrap', 'routeResolverServices']);

    app.config(['$stateProvider', 'routeResolverProvider', '$controllerProvider',
        '$provide',
        function ($stateProvider, routeResolverProvider, $controllerProvider, $provide) {
            app.register = {
                controller: $controllerProvider.register,
                factory: $provide.factory,
                service: $provide.service
            };
            var route = routeResolverProvider.route;
            $stateProvider
                .state('userprofile', route.resolve('userProfileController', 'userProfile', 'user', 'userPfileCtrl', false))
                .state('state', route.resolve('defaultController', 'default', '', 'defaultCtrl', false))
                .state('error', {
                    templateUrl: 'views/error.html'
                })


        }
    ]);
    return app;

})
