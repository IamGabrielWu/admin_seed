/**
 * Created by whoami on 16/8/4.
 */
define(['app'], function (admin_seed) {
    var injectParams = ['httpService'];
    var defaultController = function (httpService) {
        var defaultCtrl = this
        defaultCtrl.content={
            title:'This is default title',
            body:'This is default body',
            foot:'This is default foot'
        }
    }
    defaultController.$inject = injectParams;
    admin_seed.register.controller('defaultController', defaultController);
})
