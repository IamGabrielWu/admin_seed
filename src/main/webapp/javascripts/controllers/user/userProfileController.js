/**
 * Created by whoami on 16/8/4.
 */
define(['app'], function (admin_seed) {
    var injectParams = ['httpService'];
    var userProfileController = function (httpService) {
        var userPfileCtrl = this
        userPfileCtrl.content={
            title:'This is title',
            body:'This is body',
            foot:'This is foot'
        }
    }
    userProfileController.$inject = injectParams;
    admin_seed.register.controller('userProfileController', userProfileController);
})
