define(['app'], function (admin_seed) {
    var indexController = function (httpService) {
        var indexCtrl = this
        httpService.query("/menu/query").then(function (result) {
            if (result.status == 200) {
                var temp=[];
                angular.forEach(result.data,function(menu){
                    if(menu.parentId==0){
                        temp.push(menu)
                    }
                })
                angular.forEach(temp,function(menu){
                    menu.children=[];
                    angular.forEach(result.data,function(dataMenu){
                        if(dataMenu.parentId==menu.id){
                            menu.children.push(dataMenu)
                        }
                    })
                })
                indexCtrl.menuList = temp;
                console.log(indexCtrl.menuList)
            }
        })
    }
    admin_seed.controller('indexController', indexController);
})
