package admin.seed.menu.ctrl;

import admin.seed.menu.domain.Menu;
import admin.seed.menu.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by whoami on 16/8/2.
 */
@RestController
@EnableAutoConfiguration
@RequestMapping("/menu")
public class MenuController {

    @Autowired
    private MenuService menuService;

    @RequestMapping(value="/query",method = RequestMethod.GET)
    public List<Menu> query(){
        return menuService.query();
    }

    @RequestMapping(value="/findById/{id}",method = RequestMethod.GET)
    public Menu findById(@PathVariable("id")Long id){
//        System.out.println("testing hot deploy == >\n\n\n\n"+id);
        return menuService.findById(id);
    }

    @RequestMapping(value="/findByParentId/{parentId}",method = RequestMethod.GET)
    public List<Menu> findByParentId(@PathVariable("parentId") Long parentId){
        return menuService.findAllChildren(parentId);
    }


}
