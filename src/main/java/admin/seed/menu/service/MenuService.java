package admin.seed.menu.service;

import admin.seed.menu.domain.Menu;

import java.util.List;

/**
 * Created by whoami on 16/8/2.
 */
public interface MenuService {

    void save(Menu menu);
    List<Menu> query();
    Menu findById(long id);
    void updateMenu(Menu menu);
    List<Menu> findAllChildren(long id);
}
