package admin.seed.menu.service;

import admin.seed.menu.domain.Menu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by whoami on 16/8/2.
 */
@Service
public class MenuServiceImpl implements  MenuService{

    @Autowired
    private MongoOperations mongoOperations;

    @Override
    public void save(Menu menu) {
        mongoOperations.save(menu);
    }

    @Override
    public List<Menu> query() {
        return mongoOperations.findAll(Menu.class);
    }

    @Override
    public Menu findById(long id) {
        return mongoOperations.findById(id,Menu.class);

    }

    @Override
    public void updateMenu(Menu menu) {
//        mongoOperations.update
    }

    @Override
    public List<Menu> findAllChildren(long id) {
        return mongoOperations.find(new Query(Criteria.where("parentId").is(id)),Menu.class);
    }
}
