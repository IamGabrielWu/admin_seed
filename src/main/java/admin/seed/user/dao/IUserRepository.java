package admin.seed.user.dao;

import admin.seed.user.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by whoami on 16/8/2.
 */
public interface IUserRepository extends MongoRepository<User, Long> {

    User findByUsername(String username);


}