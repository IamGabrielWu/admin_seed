package admin.seed;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdminSeedApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdminSeedApplication.class, args);
	}
}
